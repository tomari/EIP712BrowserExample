# Signing with EIP-712 using Metamask

This is a small example single-page app that signs a message using `eth_signTypedData_v4` in browser using MetaMask.
The program does not depend on external libraries, and talks directly to MetaMask extension.

The signed message should be verified using [eth-sig-util](https://github.com/MetaMask/eth-sig-util).
