function View(){
	this.attachConnectHandler=this.attachHandler.bind(this,'button_connect');
	this.attachSignHandler=this.attachHandler.bind(this,'button_sign');
	this.setAccountId=this.setText.bind(this,'accountid');
	this.setChainId=this.setText.bind(this,'chainid');
}
View.prototype.attachHandler=function(elementId,handlerFn){
	let element=document.getElementById(elementId);
	element.addEventListener('click',function(e){
		e.preventDefault();
		handlerFn();
	});
};
View.prototype.setText=function(elementId,textContent){
	let element=document.getElementById(elementId);
	while(element.childNodes.length>0){
		element.removeChild(element.firstChild);
	}
	element.appendChild(document.createTextNode(textContent));
};
View.prototype.showResult=function(messagebody,signature){
	this.setText('messagebody',messagebody);
	this.setText('signature',signature);
};
function Model(){
	this.accounts=[];
}
Model.prototype.Connect=function(){
	if(window.ethereum){
		window.ethereum.on('disconnect',this.onDisconnect.bind(this));
		window.ethereum.on('accountsChanged',this.onAccountsChanged.bind(this));
		window.ethereum.on('chainChanged',this.onChainChanged.bind(this));
		window.ethereum.request({ method: 'eth_requestAccounts' })
		.then((accounts)=>{
			this.accounts=accounts;
		})
		.then(window.ethereum.request.bind(window.ethereum,{ method: 'eth_chainId' }))
		.then((chainId)=>{
			this.chain=chainId;
			this.callConnectionChangedHandler();
		})
		.catch((err)=>{
			console.log(err);
		});
	}
};
Model.prototype.onDisconnect=function(){
	console.log('Disconnected');
	this.connected=false;
	this.callConnectionChangedHandler();
};
Model.prototype.onAccountsChanged=function(accounts){
	console.log('Accounts changed '+JSON.stringify(accounts));
	this.accounts=accounts;
	this.callConnectionChangedHandler();
};
Model.prototype.onChainChanged=function(chainid){
	console.log('Chain changed '+chainid);
	this.chain=chainid;
	this.callConnectionChangedHandler();
};
Model.prototype.setConnectionChangedHandler=function(handler){
	this.connectionChangedHandler=handler;
};
Model.prototype.setSignCompleteHandler=function(handler){
	this.signCompleteHandler=handler;
};
Model.prototype.callConnectionChangedHandler=function(){
	if(this.connectionChangedHandler){
		this.connectionChangedHandler(this.accounts.length>0,this.accounts,this.chain);
	}
};
Model.prototype.SignMessage=function(){
	if(!(window.ethereum && this.accounts.length>0)){
		console.log('Metamask not found or not connected');
		return;
	}
	const messagebody=JSON.stringify(
		{
			domain: {
				chainId: this.chain,
				name: 'Arigato Java',
				version: '1'
			},
			message: {
				text:'Hello World'
			},
			primaryType: 'Message',
			types: {
				EIP712Domain: [
					{ name: 'name', type: 'string' },
					{ name: 'version', type: 'string' },
					{ name: 'chainId', type: 'uint256' }
				],
				Message: [
					{ name: 'text', type: 'string' },
				]
			}
		}
	);
	window.ethereum.request({
		method: 'eth_signTypedData_v4',
		params: [
			this.accounts[0],
			messagebody
		]
	})
	.then((res)=>{
		if(this.signCompleteHandler){
			this.signCompleteHandler(messagebody,res);
		}
		console.log(res);
	})
	.catch((err)=>{
		console.log(err);
	});
};
function Controller(view,model){
	this.view=view;
	this.model=model;
}
Controller.prototype.init=function(){
	this.model.setConnectionChangedHandler(this.OnConnectionChanged.bind(this));
	this.model.setSignCompleteHandler(this.view.showResult.bind(this.view));
	this.view.attachConnectHandler(this.model.Connect.bind(this.model));
	this.view.attachSignHandler(this.model.SignMessage.bind(this.model));
};
Controller.prototype.OnConnectionChanged=function(connected,accounts,chain){
	this.view.setAccountId(connected?JSON.stringify(accounts):'None');
	this.view.setChainId(connected?chain:'None');
};
window.onload=function(){
	let v=new View();
	let m=new Model();
	window.app=new Controller(v,m);
	window.app.init();
};
